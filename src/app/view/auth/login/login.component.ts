import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { from } from 'rxjs';
import { LoginController } from "src/app/controller/auth/auth.controller";
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formInputData: FormGroup;
  login:any={};
  submitted = false;
  error = null;
  constructor(private formBuilder: FormBuilder,
    private controller:LoginController,
    private toastr:ToastrService,
    private router:Router) { }

  ngOnInit(): void {
    if (localStorage.getItem('token') != null)
      this.router.navigateByUrl('/home');
      this.formInputData = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
  });
  }

  get validation() { return this.formInputData.controls; }

  onSubmit(){
    this.submitted = true;
    this.controller.login(this.login).subscribe(
      (res: any) => {
        console.log(res);
        localStorage.setItem('token', res.access_token);
        this.router.navigateByUrl('/home');
      },
      err => {
        if (err.status == 400)
          this.toastr.error('Incorrect username or password.', 'Authentication failed.');
        else
          console.log(err);
      }
    );
  }
  handleError(error) {
    this.error = error.error.error;
  }
}
