import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ProductController } from 'src/app/controller/productcontroller';

@Component({
  selector: 'app-products-create',
  templateUrl: './products-create.component.html',
  styleUrls: ['./products-create.component.css']
})
export class ProductsCreateComponent implements OnInit {

  product:any={};
  formInputData: FormGroup;
  submitted = false;
  proimage: File = null;

  constructor(private controller: ProductController,
    private router: Router,
    private formBuilder: FormBuilder,
    private toastr:ToastrService) { }
  ngOnInit(): void {
      this.formInputData = this.formBuilder.group({
      barcode: ['', Validators.required],
      title: ['', Validators.required],
      titlekh: ['', Validators.required],
      category_id: ['', Validators.required],
      supplier_id: ['', Validators.required],
      qtyonhand: ['', Validators.required],
      qtyalert: ['', Validators.required],
      proimage: ['', Validators.required],
  });
  }
  get validation() { return this.formInputData.controls; }

  onSubmit(): void {
    this.submitted = true;
    console.log(this.product);
    // this.controller.create(this.product).subscribe(() => {
    //   this.toastr.success("your work successfully !!", "Your do something");
    //   this.router.navigate(['/product']);
    // })

  }

  fileProgress(fileInput: any) {
    <File>fileInput.target.files[0];
  }


  cancel(): void {
    this.router.navigate(['/product'])
  }

}
