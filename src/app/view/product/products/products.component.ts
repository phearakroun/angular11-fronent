import { Component, OnInit } from '@angular/core';
import { ProductController } from 'src/app/controller/productcontroller';
import { ProductModel } from 'src/app/model/productmodel';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  resData: ProductModel[]
  current_page: 1;
  seaching:string;
  config = {
    itemsPerPage: [],
    currentPage: [],
    totalItems: []
  }; 
  constructor(private controller: ProductController) { }
  ngOnInit(): void {
    this.getData(); 
  }
  getData():void{
      this.controller.read(this.current_page).subscribe(res => {
      this.resData=res['data'];
      this.config.itemsPerPage=res['meta']['per_page'];
      this.config.totalItems=res['meta']['total'];
      this.config.currentPage=res['meta']['current_page'];
    })
  }

  pageChanged(event:any){
    this.controller.read(this.config.currentPage = event).subscribe(res => {
      this.resData = res["data"];
    })
  }
 
  

}
