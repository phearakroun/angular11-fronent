import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { from } from 'rxjs';
import { CategoryController } from "src/app/controller/categorycontroller";
import { CategoryModel } from "src/app/model/categorymodel";
import { ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-categories-edit',
  templateUrl: './categories-edit.component.html',
  styleUrls: ['./categories-edit.component.css']
})
export class CategoriesEditComponent implements OnInit {
  category:CategoryModel={
    name:"",
    namekh:""
  };
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private controller:CategoryController,
    private toastr:ToastrService
    ) { }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get("id");
    this.controller.edit(id).subscribe((res) => {
      this.category = res['data'];
    });
  }

  cancel(): void {
    this.router.navigate(['/categories'])
  }
  update(): void {
    this.controller.update(this.category).subscribe(() => {
      this.toastr.success("your work successfully !!", "Your do something");
      this.router.navigate(["/categories"]);
    });
  }

}
