import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {CategoryController } from "src/app/controller/categorycontroller";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-categories-create',
  templateUrl: './categories-create.component.html',
  styleUrls: ['./categories-create.component.css']
})
export class CategoriesCreateComponent implements OnInit {

  category:any={};
  formInputData: FormGroup;
  submitted = false;
  
  constructor(private controller: CategoryController,
    private router: Router,
    private formBuilder: FormBuilder,
    private toastr:ToastrService) { }
  ngOnInit(): void {
    this.formInputData = this.formBuilder.group({
      name: ['', Validators.required],
      namekh: ['', Validators.required],
  });
  }
  get validation() { return this.formInputData.controls; }

  onSubmit(): void {
    this.submitted = true;
    this.controller.create(this.category).subscribe(() => {
      this.toastr.success("your work successfully !!", "Your do something");
      this.router.navigate(['/categories']);
    })

  }

  cancel(): void {
    this.router.navigate(['/categories'])
  }
}
