import { Component, OnInit } from '@angular/core';
import { CategoryController } from 'src/app/controller/categorycontroller'
import { CategoryModel } from "src/app/model/categorymodel";
import Swal from 'sweetalert2';
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  resData: CategoryModel[]
  current_page: 1;
  seaching:string;
  config = {
    itemsPerPage: [],
    currentPage: [],
    totalItems: []
  }; 
  constructor(
    private controller: CategoryController,
    private toastr:ToastrService
    ) { }

  ngOnInit(): void {
    this.getData(); 
  }

  getData():void{
  //   var reqHeader = new HttpHeaders({ 
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token'))
  //  });

    this.controller.read(this.current_page).subscribe(res => {
      this.resData = res['data'];
      this.config.itemsPerPage=res['per_page'];
      this.config.totalItems=res['total'];
      this.config.currentPage=res['current_page'];
    })
  }
  bsearch():void{
    this.controller.search(this.seaching).subscribe(res => {
      this.resData = res["data"];
    })
  }
  pageChanged(event){
    this.controller.read(this.config.currentPage = event).subscribe(res => {
      this.resData = res["data"];
    })
  }
  delete(id:number): void {
    
    Swal.fire({
      title: 'Are you sure?',
      text: "You delete this item",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.toastr.success("your work successfully !!", "Your do something");
        this.controller.delete(id).subscribe(() => {
          this.getData();
      //   for(let i = 0; i < this.resData.length; ++i){
      //     if (this.resData[i].id === id) {
      //       this.resData.splice(i,1);
      //     }
      // }
    });
        
      }
    })
  }


}
