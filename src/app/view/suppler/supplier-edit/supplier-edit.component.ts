import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SupplierController } from 'src/app/controller/supplercontroller';
import { SupplierModel } from 'src/app/model/suppliermode';

@Component({
  selector: 'app-supplier-edit',
  templateUrl: './supplier-edit.component.html',
  styleUrls: ['./supplier-edit.component.css']
})
export class SupplierEditComponent implements OnInit {

  formData:SupplierModel={ name:"",gender:"",address:"",phone:"",email:""};
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private controller:SupplierController,
    private toastr:ToastrService
    ) { }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get("id");
    this.controller.edit(id).subscribe((res) => {
      this.formData = res['data'];
      console.log(res);
    });
  }
  cancel(): void {
    this.router.navigate(['/supplier'])
  }
  update(): void {
    this.controller.update(this.formData).subscribe(() => {
      this.toastr.success("your work successfully !!", "Your do something");
      this.router.navigate(["/supplier"]);
    });
  }

}
