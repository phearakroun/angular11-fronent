import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { SupplierController } from 'src/app/controller/supplercontroller';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.css']
})
export class SupplierComponent implements OnInit {

  
  resData: []
  current_page: 1;
  seaching:string;
  config = {
    itemsPerPage: [],
    currentPage: [],
    totalItems: []
  }; 
  constructor(
    private controller: SupplierController,
    private toastr:ToastrService
    ) { }

  ngOnInit(): void {
    this.getData(); 
  }

  getData():void{
    this.controller.read(this.current_page).subscribe(res => {
      this.resData = res['data'];
      this.config.itemsPerPage=res['per_page'];
      this.config.totalItems=res['total'];
      this.config.currentPage=res['current_page'];
    })
  }
  bsearch():void{
    this.controller.search(this.seaching).subscribe(res => {
      this.resData = res["data"];
    })
  }
  pageChanged(event){
    this.controller.read(this.config.currentPage = event).subscribe(res => {
      this.resData = res["data"];
    })
  }
  delete(id:number): void {
    
    Swal.fire({
      title: 'Are you sure?',
      text: "You delete this item",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.toastr.success("your work successfully !!", "Your do something");
        this.controller.delete(id).subscribe(() => {
          this.getData();
      //   for(let i = 0; i < this.resData.length; ++i){
      //     if (this.resData[i].id === id) {
      //       this.resData.splice(i,1);
      //     }
      // }
    });
        
      }
    })
  }

}
