import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SupplierController } from 'src/app/controller/supplercontroller';

@Component({
  selector: 'app-supplier-create',
  templateUrl: './supplier-create.component.html',
  styleUrls: ['./supplier-create.component.css']
})
export class SupplierCreateComponent implements OnInit {

  formData:any={};
  formInputData: FormGroup;
  submitted = false;
  
  constructor(private controller: SupplierController,
    private router: Router,
    private formBuilder: FormBuilder,
    private toastr:ToastrService) { }
  ngOnInit(): void {
    this.formInputData = this.formBuilder.group({
      name: ['', Validators.required],
      gender: ['', Validators.required],
      address: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', Validators.required],
  });
  }
  get validation() { return this.formInputData.controls; }
  onSubmit(): void {
    this.submitted = true;
    this.controller.create(this.formData).subscribe(() => {
      this.toastr.success("your work successfully !!", "Your do something");
      this.router.navigate(['/supplier']);
    })
  }
  cancel(): void {
    this.router.navigate(['/supplier'])
  }

}
