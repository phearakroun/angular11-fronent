import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-toastr',
  templateUrl: './toastr.component.html',
  styleUrls: ['./toastr.component.css']
})
export class ToastrComponent implements OnInit {

  constructor(private toastr: ToastrService) { }

  ngOnInit(): void {
  }
  
  
  showToasterSuccess(){
      this.toastr.success("your work successfully !!", "Your do something")
  }
  
  showToasterError(){
      this.toastr.error("your work error !!", "Your do something")
  }
  
  showToasterInfo(){
      this.toastr.info("your work info !!", "Your do something")
  }
  
  showToasterWarning(){
      this.toastr.warning("your work warning !!", "Your do something")
  }
}
