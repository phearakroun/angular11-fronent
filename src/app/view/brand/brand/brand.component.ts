import { Component, OnInit } from '@angular/core';
import { BrandController } from 'src/app/controller/brandcontroller';
import { BrandModel } from 'src/app/model/brandmodel';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css']
})
export class BrandComponent implements OnInit {
  resData:BrandModel[];
  constructor(private controller:BrandController) {}
  ngOnInit(): void {
      this.getData();
  }

  getData():void{
    this.controller.read().subscribe(res => {
      this.resData=res["data"];
       console.log(res);
    })
  }
}
