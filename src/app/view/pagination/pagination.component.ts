import { Component, OnInit } from '@angular/core';
import { PaginationController } from 'src/app/controller/paginationcontroller'
import { CategoryModel } from "src/app/model/categorymodel";

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
  resData: CategoryModel[];
  current_page: 1;
  seaching:string;
  config = {
    itemsPerPage: [],
    currentPage: [],
    totalItems: []
  }; 
  constructor(
    private controller: PaginationController
    ) { }

  ngOnInit(): void {
    this.controller.read(this.current_page).subscribe(res => {
      this.resData = res["data"];
      this.config.itemsPerPage=res['per_page'];
      this.config.totalItems=res['total'];
      this.config.currentPage=res['current_page'];
    });
  }

  bsearch():void{
    this.controller.search(this.seaching).subscribe(res => {
      this.resData = res["data"];
    })
  }
  pageChanged(event){
    this.controller.read(this.config.currentPage = event).subscribe(res => {
      this.resData = res["data"];
    })
  }
  
  
}
