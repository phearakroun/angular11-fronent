import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CategoriesComponent } from './view/categoy/categories/categories.component';
import { CategoriesCreateComponent } from './view/categoy/categories-create/categories-create.component';
import { CategoriesEditComponent } from './view/categoy/categories-edit/categories-edit.component';
import { ProductsComponent } from './view/product/products/products.component';
import { ProductsCreateComponent } from './view/product/products-create/products-create.component';
import { ProductsEditComponent } from './view/product/products-edit/products-edit.component';
import { HomeComponent} from "./view/home/home.component";
import { ErrorComponent } from './view/error/error.component';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import { PaginationComponent } from './view/pagination/pagination.component';
import { ValidationformComponent } from './view/validationform/validationform.component';
import { ToastrComponent } from './view/toastr/toastr.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { LoginComponent } from './view/auth/login/login.component';
import { BrandComponent } from './view/brand/brand/brand.component';
import { SupplierComponent } from './view/suppler/supplier/supplier.component';
import { SupplierCreateComponent } from './view/suppler/supplier-create/supplier-create.component';
import { SupplierEditComponent } from './view/suppler/supplier-edit/supplier-edit.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LanguageInterceptor } from './helper/language.interceptor';
import { UploadComponent } from './view/upload/upload.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    CategoriesComponent,
    CategoriesCreateComponent,
    CategoriesEditComponent,
    ProductsComponent,
    ProductsCreateComponent,
    ProductsEditComponent,
    HomeComponent,
    ErrorComponent,
    PaginationComponent,
    ValidationformComponent,
    ToastrComponent,
    LoginComponent,
    BrandComponent,
    SupplierComponent,
    SupplierCreateComponent,
    SupplierEditComponent,
    UploadComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    // FormInpu===========================
    FormsModule,
    // Pagination=================
    NgxPaginationModule,
    // Validation==============
    ReactiveFormsModule,
    // Toastr========================
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  providers: [
    {
      provide:HTTP_INTERCEPTORS,
      useClass:LanguageInterceptor,
      multi:true
    },
    HttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
