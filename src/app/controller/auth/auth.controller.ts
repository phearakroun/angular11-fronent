import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { UserModel } from "src/app/model/usermodel";
import {BaseConnection } from "src/app/service/connection.service";

@Injectable({
  providedIn: "root",
})
export class LoginController {

  constructor(private http: HttpClient,private cn:BaseConnection) {}

  login(user: UserModel): Observable<UserModel> {
    return this.http.post<UserModel>(this.cn.baseUrl+"/api/auth/login", user).pipe(
      map((obj) => obj),
    );
  }

}
