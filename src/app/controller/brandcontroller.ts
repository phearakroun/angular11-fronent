import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { BrandModel } from "src/app/model/brandmodel";
import { BaseConnection } from '../service/connection.service';

@Injectable({
  providedIn: "root",
})
export class BrandController {
  
  constructor(private http: HttpClient,private cn:BaseConnection) {}
  
  read(): Observable<BrandModel[]> {
    return this.http.get<BrandModel[]>(this.cn.ibaseurl).pipe(
      map((obj) => obj)
    );
  }
}
