import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import {BaseConnection } from "src/app/service/connection.service";
import { ProductModel } from '../model/productmodel';

@Injectable({
  providedIn: "root",
})
export class ProductController {

  constructor(private http: HttpClient,private cn:BaseConnection) {}
  tokenHeader={
      headers: {
        Authorization: 'Bearer '+localStorage.getItem('token'),
      }
  }

  read(page:number): Observable<ProductModel[]> {
    return this.http.get<ProductModel[]>(this.cn.baseUrl+"/api/auth/product?page="+page,this.tokenHeader).pipe(
      map((obj) => obj)
    );
  }

  // search(keysearch:string): Observable<CategoryModel[]> {
  //   return this.http.get<CategoryModel[]>(this.cn.baseUrl+"/api/auth/categories/search/"+keysearch,this.tokenHeader).pipe(
  //     map((obj) => obj)
  //   );
  //   }

  create(product: ProductModel): Observable<ProductModel> {
    return this.http.post<ProductModel>(this.cn.baseUrl+"/api/auth/product", product,this.tokenHeader).pipe(
      map((obj) => obj),
    );
  }

  // edit(id: number): Observable<CategoryModel> {
  //   return this.http.get<CategoryModel>(this.cn.baseUrl+"/api/auth/categories/"+id,this.tokenHeader).pipe(
  //     map((obj) => obj),
  //   );
  // }

  // update(category: CategoryModel): Observable<CategoryModel> {
  //   return this.http.put<CategoryModel>(this.cn.baseUrl+"/api/auth/categories/"+category.id, category,this.tokenHeader).pipe(
  //     map((obj) => obj),
  //   );
  // }

  // delete(id: number): Observable<CategoryModel> {
  //   return this.http.delete<CategoryModel>(this.cn.baseUrl+"/api/auth/categories/"+id,this.tokenHeader).pipe(
  //     map((obj) => obj),
  //   );
  // }
}
