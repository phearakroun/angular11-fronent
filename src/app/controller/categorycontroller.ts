import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { CategoryModel } from "src/app/model/categorymodel";
import {BaseConnection } from "src/app/service/connection.service";

@Injectable({
  providedIn: "root",
})
export class CategoryController {

  constructor(private http: HttpClient,private cn:BaseConnection) {}
  tokenHeader={
      headers: {
        Authorization: 'Bearer '+localStorage.getItem('token'),
      }
  }

  read(page:number): Observable<CategoryModel[]> {
    return this.http.get<CategoryModel[]>(this.cn.baseUrl+"/api/auth/categories?page="+page,this.tokenHeader).pipe(
      map((obj) => obj)
    );
  }

  search(keysearch:string): Observable<CategoryModel[]> {
    return this.http.get<CategoryModel[]>(this.cn.baseUrl+"/api/auth/categories/search/"+keysearch,this.tokenHeader).pipe(
      map((obj) => obj)
    );
    }

  create(categories: CategoryModel): Observable<CategoryModel> {
    return this.http.post<CategoryModel>(this.cn.baseUrl+"/api/auth/categories", categories,this.tokenHeader).pipe(
      map((obj) => obj),
    );
  }

  edit(id: number): Observable<CategoryModel> {
    return this.http.get<CategoryModel>(this.cn.baseUrl+"/api/auth/categories/"+id,this.tokenHeader).pipe(
      map((obj) => obj),
    );
  }

  update(category: CategoryModel): Observable<CategoryModel> {
    return this.http.put<CategoryModel>(this.cn.baseUrl+"/api/auth/categories/"+category.id, category,this.tokenHeader).pipe(
      map((obj) => obj),
    );
  }

  delete(id: number): Observable<CategoryModel> {
    return this.http.delete<CategoryModel>(this.cn.baseUrl+"/api/auth/categories/"+id,this.tokenHeader).pipe(
      map((obj) => obj),
    );
  }
}
