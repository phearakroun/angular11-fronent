import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { CategoryModel } from "src/app/model/categorymodel";
import {BaseConnection } from "src/app/service/connection.service";

@Injectable({
  providedIn: "root",
})
export class PaginationController {

  constructor(private http: HttpClient,private cn:BaseConnection) {}

  read(page:number): Observable<CategoryModel[]> {
    return this.http.get<CategoryModel[]>(this.cn.baseUrl+"/api/pagination?page="+page).pipe(
      map((obj) => obj)
    );
    }

    search(keysearch:string): Observable<CategoryModel[]> {
      return this.http.get<CategoryModel[]>(this.cn.baseUrl+"/api/pagination/search/"+keysearch).pipe(
        map((obj) => obj)
      );
      }
}
