import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import {BaseConnection } from "src/app/service/connection.service";
import { UploadModel } from '../model/uploadmodel';

@Injectable({
  providedIn: "root",
})
export class UploadController {

  constructor(private http: HttpClient,private cn:BaseConnection) {}

  create(product: UploadModel): Observable<UploadModel> {
    return this.http.post<UploadModel>(this.cn.baseUrl+"/api/auth/product", product).pipe(
      map((obj) => obj),
    );
  }
}
