import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import {BaseConnection } from "src/app/service/connection.service";
import { SupplierModel } from '../model/suppliermode';

@Injectable({
  providedIn: "root",
})
export class SupplierController {

  constructor(private http: HttpClient,private cn:BaseConnection) {}
  tokenHeader={
      headers: {
        Authorization: 'Bearer '+localStorage.getItem('token'),
      }
  }

  read(page:number): Observable<SupplierModel[]> {
    return this.http.get<SupplierModel[]>(this.cn.baseUrl+"/api/auth/supplier?page="+page,this.tokenHeader).pipe(
      map((obj) => obj)
    );
  }

  search(keysearch:string): Observable<SupplierModel[]> {
    return this.http.get<SupplierModel[]>(this.cn.baseUrl+"/api/auth/supplier/search/"+keysearch,this.tokenHeader).pipe(
      map((obj) => obj)
    );
    }

  create(idata: SupplierModel): Observable<SupplierModel> {
    return this.http.post<SupplierModel>(this.cn.baseUrl+"/api/auth/supplier", idata,this.tokenHeader).pipe(
      map((obj) => obj),
    );
  }

  edit(id: number): Observable<SupplierModel> {
    return this.http.get<SupplierModel>(this.cn.baseUrl+"/api/auth/supplier/"+id,this.tokenHeader).pipe(
      map((obj) => obj),
    );
  }

  update(idata: SupplierModel): Observable<SupplierModel> {
    return this.http.put<SupplierModel>(this.cn.baseUrl+"/api/auth/supplier/"+idata.id, idata,this.tokenHeader).pipe(
      map((obj) => obj),
    );
  }

  delete(id: number): Observable<SupplierModel> {
    return this.http.delete<SupplierModel>(this.cn.baseUrl+"/api/auth/supplier/"+id,this.tokenHeader).pipe(
      map((obj) => obj),
    );
  }
}
