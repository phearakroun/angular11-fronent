import { Component } from '@angular/core';
import { from } from 'rxjs';
import { Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'web-app';
  lang:any;
  constructor(private translateService:TranslateService,private router: Router){
    this.translateService.setDefaultLang('en');
    this.translateService.use(localStorage.getItem('lang')|| "en");
  }
  ngOnInit(){
    this.lang=localStorage.getItem("lang") || "en";
  }
  changeLang(lang){
    localStorage.setItem("lang",lang);
    window.location.reload();
  }
  onLogout() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

}
