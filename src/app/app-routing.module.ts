import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriesCreateComponent } from './view/categoy/categories-create/categories-create.component';
import { CategoriesComponent } from './view/categoy/categories/categories.component';
import { ErrorComponent } from './view/error/error.component';
import { HomeComponent } from './view/home/home.component';
import { from } from 'rxjs';
import { CategoriesEditComponent } from './view/categoy/categories-edit/categories-edit.component';
import { PaginationComponent } from './view/pagination/pagination.component';
import { ValidationformComponent } from './view/validationform/validationform.component';
import { ToastrComponent } from './view/toastr/toastr.component';
import { LoginComponent } from './view/auth/login/login.component';
import { AuthGuard } from "src/app/controller/auth/auth.guard";
import { BrandComponent } from './view/brand/brand/brand.component';
import { SupplierComponent } from './view/suppler/supplier/supplier.component';
import { SupplierCreateComponent } from './view/suppler/supplier-create/supplier-create.component';
import { SupplierEditComponent } from './view/suppler/supplier-edit/supplier-edit.component';
import { ProductsComponent } from './view/product/products/products.component';
import { ProductsCreateComponent } from './view/product/products-create/products-create.component';
import { UploadComponent } from './view/upload/upload.component';

const routes: Routes = [
  {path:"",redirectTo:"login",pathMatch:"full"},
  {path:"home",component:HomeComponent,canActivate:[AuthGuard]},
  {path:"categories",component:CategoriesComponent,canActivate:[AuthGuard]},
  {path:"category-create",component:CategoriesCreateComponent,canActivate:[AuthGuard]},
  {path:"category-edit/:id",component:CategoriesEditComponent,canActivate:[AuthGuard]},
  {path:"pagination",component:PaginationComponent,canActivate:[AuthGuard]},
  {path: "validation",component:ValidationformComponent,canActivate:[AuthGuard]},
  {path: "toastr",component:ToastrComponent,canActivate:[AuthGuard]},
  {path:"login",component:LoginComponent},
  {path:"brand",component:BrandComponent},
  {path:"supplier",component:SupplierComponent,canActivate:[AuthGuard]},
  {path:"supplier-create",component:SupplierCreateComponent,canActivate:[AuthGuard]},
  {path:"supplier-edit/:id",component:SupplierEditComponent,canActivate:[AuthGuard]},
  {path:"product",component:ProductsComponent,canActivate:[AuthGuard]},
  {path:"product-create",component:ProductsCreateComponent},
  {path:"upload",component:UploadComponent},
  {path:"**",component:ErrorComponent},
  
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
