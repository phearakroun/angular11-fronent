export class SupplierModel{
    id?:number
    name:string
    gender:string
    address:string
    phone:string
    email:string
}