export class ProductModel{
    id?:number
    proimage:string
    category_id:number
    supplier_id:number
    barcode:string
    title:string
    titlekh:string
    qtyonhand:number
    qtyalert:number
}